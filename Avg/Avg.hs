{-
   Exercise: Clipping for average

   We will use the (well known and used) Adult dataset:

   https://archive.ics.uci.edu/ml/datasets/Adult

   Check the file adult.csv, which format is a table with the following columns:

   age,workclass,fnlwgt,edication,education-num,marital-status,occupation,relationship,race,sex
   ,capital-gain,capital-loss,hours-per-wee,native-country,income
-}
{-# LANGUAGE Safe #-}
{-# LANGUAGE DataKinds #-}
module Avg where

import RowDef
import AnalystLP

{- Exercise

   a) You have learnt in the lectures that Avg has sensitivity 2. To
   enforce that, the function works only for numbers between -1 and 1, but
   what happens if your dataset contains data outside such interval?

   In this task, you are asked to write a function which takes human ages (where
   the maximum value is 122 years old) and map them into the interval -1 and 1
   and viceversa.

   You should write the following function:

   toInterval   :: Age -> Double
   fromInterval :: Double -> Age

   b) Do your functions above fulfill the following property?

   test = map (fromInterval . toInterval) [1..122]

   If not, you should fix your implementation.

   c) Now, let's write an analysis which calculates the average age of the adult
      dataset.

      analysis :: Epsilon -> Dataset 1 Row -> Query (Value Double)

      To implement this function, you need to use `dpAvg`.

   d)_

-}

maxAge = 122

toInterval :: Age -> Double
toInterval age = -1 + 2*((fromInteger . toInteger) age)/maxAge
{-
Proof:
 age <= maxVal
 age/maxVal <= 1
 0  <= age/maxVal        <= 1
 0  <= 2*age/maxVal      <= 2
 -1 <= -1 + 2*age/maxVal <= 1
-}
ages :: [Int]
ages = [15, 53, 81, 27, 33, 122]

agesRange = map toInterval ages

fromInterval :: Double -> Age
{- This one is bad: it doesn't fill the property
   fromInterval n = floor $ maxAge * (n+1) / 2

    ages == map (fromInterval . toInterval) ages
-}
fromInterval n = round $ maxAge * (n+1) / 2

{-
Proof:
 n = -1 + 2*age/maxVal
 n + 1 = 2*age/maxVal
 (n+1)/2 = age/maxVal
 maxVal * (n+1) / 2 = age
-}

idAges = map (fromInterval . toInterval) ages

test = map (fromInterval . toInterval) [1..122]

-- We need something that adds a constant, the alpha changes accordingly
avgAdult :: Epsilon -> Dataset 1 Row -> Query (Value Double)
avgAdult = dpAvg (toInterval . age)

analysis = avgAdult 10
