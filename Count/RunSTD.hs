{-# LANGUAGE Unsafe #-}
{-# LANGUAGE DataKinds #-}
module RunSTD where

-- Definition of columns of the CSV file
import RowDef
-- Interface to run the query with a given epsilon
import CuratorLP (importCSV, dpEval)
 -- Analyses
import STD

loadTraffic :: IO [Row]
loadTraffic = do
  ds <- importCSV "std.csv"
  return $ toRows ds

executeSTD1 = undefined 

executeSTD10 = undefined

executeSTD30 = undefined 
