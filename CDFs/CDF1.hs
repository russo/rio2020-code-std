{-
   Exercise: computing CDFs of network packet lengths.

   Check the file hotspot.csv, which format is a table with
   the following columns:

   No.,Time,Source,Destination,Protocol,Length

   CDF version 1
-}
{-# LANGUAGE Safe #-}
{-# LANGUAGE DataKinds #-}
module CDF1 where

import RowDef (Row,Bins,size)
import AnalystLP

cdf1 :: Bins -> Epsilon -> Dataset 1 Row -> Query (Value [Double])
cdf1 bins eps dataset = undefined 

type Range = (Int,Int)

exploreCDF1 :: Range -> Epsilon -> Beta -> [(Int,Alpha)]
exploreCDF1 (i,f) eps beta = undefined 

-- Analysis to be run by the curator
analysis = undefined 
