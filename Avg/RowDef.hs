{-
   Exercise: Dealing with the Adult dataset (many exercises)

   We will use the file adult.csv
-}
{-# LANGUAGE Trustworthy #-}
{-# LANGUAGE DataKinds #-}
module RowDef
   ( -- Types
     Row ()
   , Age
   , WorkcClass
   , Fnlwget
   , Education
   , EducationNum
   , MaritalStatus
   , Occupation
   , Relationship
   , Race
   , Sex
   , CapitalGain
   , CapitalLoss
   , HoursPerWee
   , NativeCountry
   , Income
     -- Selectors
   , age
   , workclass
   , fnlwget
   , educ
   , educnum
   , maritalst
   , occupation
   , relationship
   , race
   , sex
   , gain
   , loss
   , hours
   , native
   , income
     -- Constructor
   , toRows
   )
where

type Age           = Int
type WorkcClass    = String
type Fnlwget       = Int
type Education     = String
type EducationNum  = Int
type MaritalStatus = String
type Occupation    = String
type Relationship  = String
type Race          = String
type Sex           = String
type CapitalGain   = Int
type CapitalLoss   = Int
type HoursPerWee   = Int
type NativeCountry = String
type Income        = String

data Row = Row {
    age          :: Age
  , workclass    :: WorkcClass
  , fnlwget      :: Fnlwget
  , educ         :: Education
  , educnum      :: EducationNum
  , maritalst    :: MaritalStatus
  , occupation   :: Occupation
  , relationship :: Relationship
  , race         :: Race
  , sex          :: Sex
  , gain         :: CapitalGain
  , loss         :: CapitalLoss
  , hours        :: HoursPerWee
  , native       :: NativeCountry
  , income       :: Income
  }
  deriving Show

toRow :: [String] -> Row
toRow [a, w, f, e, en, ms, o, r, ra, s, g, l, h, n, i] =
  Row {
        age          = read a
      , workclass    = w
      , fnlwget      = read f
      , educ         = e
      , educnum      = read en
      , maritalst    = ms
      , occupation   = o
      , relationship = r
      , race         = ra
      , sex          = s
      , gain         = read g
      , loss         = read l
      , hours        = read h
      , native       = n
      , income       = i
  }

toRow _ = error "Not correct format!"

toRows :: [[String]] -> [Row]
toRows csv = map toRow csv
