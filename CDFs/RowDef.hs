{-
   Exercise: computing CDFs of network packet lengths.

   Check the file hotspot.csv, which format is a table with
   the following columns:

   No.,Time,Source,Destination,Protocol,Length

   CDF version 1
-}
{-# LANGUAGE Trustworthy #-}
{-# LANGUAGE DataKinds #-}
module RowDef
   ( -- Types
     Row ()
   , IP
   , Bytes
   , Bins
     -- Selectors
   , no
   , time
   , src
   , dst
   , protocol
   , size
     -- Constructor
   , toRows
   )
where

type IP    = String
type Bytes = Int

data Row = Row {
                 no       :: Int
               , time     :: Float
               , src      :: IP
               , dst      :: IP
               , protocol :: String
               , size     :: Bytes
               }
           deriving Show

type Bins = [Bytes]

toRow :: [String] -> Row
toRow [n, t, s, d, p, bytes] = Row {
                                  no       = read n
                                , time     = read t
                                , src      = s
                                , dst      = d
                                , protocol = p
                                , size     = read bytes
                                }
toRow _ = error "Not correct format!"

toRows :: [[String]] -> [Row]
toRows csv = map toRow csv
