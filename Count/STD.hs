{-
   Exercise: Protecting 1 person privacy

   We will use the file std.csv, which format is a table with
   the following columns:

   Please, do not open that file.

   name,lastname,std
-}
{-# LANGUAGE Safe #-}
{-# LANGUAGE DataKinds #-}
module STD where

import RowDef (Row,Name,Lastname,STD,name,lastname)
import AnalystLP

isMyFriendSick :: Name -> Lastname -> Epsilon -> Dataset 1 Row -> Query (Value Double)
isMyFriendSick n l eps ds = undefined 

analysis1  = isMyFriendSick "Martin" "Baro" 1
analysis10 = isMyFriendSick "Martin" "Baro" 10
analysis30 = isMyFriendSick "Martin" "Baro" 30
