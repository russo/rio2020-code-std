
{-
   Exercise: DP-sums

-}
{-# LANGUAGE Safe #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeApplications #-}
module Sum where

import RowDef
import AnalystLP

-- Info provided by the Data Curator
sizeRange = range @60 @350

totalBytes :: Epsilon -> Dataset 1 Row -> Query (Value Double)
totalBytes eps ds = undefined

router1 = "Cisco_ba:1f:80"

bytesRouter1 :: Epsilon -> Dataset 1 Row -> Query (Value Double)
bytesRouter1 eps ds = undefined

analysis1 = totalBytes 1
analysisRouter1 = bytesRouter1 1
