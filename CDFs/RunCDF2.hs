{-# LANGUAGE Unsafe #-}
{-# LANGUAGE DataKinds #-}
module RunCDF2 where

-- Definition of columns of the CSV file
import RowDef
-- Interface to run the query with a given epsilon
import CuratorLP (importCSV, dpEval)
 -- Analyses
import CDF2

loadTraffic :: IO [Row]
loadTraffic = do
  ds <- importCSV "hotspot.csv"
  return $ toRows ds

executeCDF2 = undefined 
