{-# LANGUAGE Unsafe #-}
{-# LANGUAGE DataKinds #-}
module RunAvg where

-- Definition of columns of the CSV file
import RowDef
-- Interface to run the query with a given epsilon
import CuratorLP (importCSV, dpEval)
 -- Analyses
import Avg

loadAdult :: IO [Row]
loadAdult = do
  ds <- importCSV "adult.csv"
  return $ toRows ds

executeAvg = do
  ds <- loadAdult
  dpEval analysis ds 10
