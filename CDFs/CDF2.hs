{-
   Exercise: computing CDFs of network packet lengths.

   Check the file hotspot.csv, which format is a table with
   the following columns:

   No.,Time,Source,Destination,Protocol,Length

   CDF version 2
-}
{-# LANGUAGE Safe #-}
{-# LANGUAGE DataKinds #-}
module CDF2 where

import RowDef (Row,Bins,size)
import AnalystLP
import CDF1 (Range,exploreCDF1,cdf1)
import qualified Data.Map.Lazy as Map

-- | Assign data @x@ to its respective bin from @bins@
assignBin :: Ord a => [a] -> a -> a
assignBin bins x = undefined 

cdf2 :: Bins -> Epsilon -> Dataset 1 Row -> Query (Value [Double])
cdf2 bins eps dataset = undefined 

exploreCDF2 :: Range -> Epsilon -> Beta -> [(Int,Alpha)]
exploreCDF2 (i,f) eps beta = undefined 

compareCDFs :: Range -> Epsilon -> Beta -> [(Int,String)]
compareCDFs (i,f) eps beta = undefined 

-- Analysis to be run by the curator
analysis = undefined 
