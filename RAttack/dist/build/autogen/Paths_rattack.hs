{-# LANGUAGE CPP #-}
{-# LANGUAGE NoRebindableSyntax #-}
{-# OPTIONS_GHC -fno-warn-missing-import-lists #-}
module Paths_rattack (
    version,
    getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir,
    getDataFileName, getSysconfDir
  ) where

import qualified Control.Exception as Exception
import Data.Version (Version(..))
import System.Environment (getEnv)
import Prelude

#if defined(VERSION_base)

#if MIN_VERSION_base(4,0,0)
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#else
catchIO :: IO a -> (Exception.Exception -> IO a) -> IO a
#endif

#else
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#endif
catchIO = Exception.catch

version :: Version
version = Version [0,1,0,0] []
bindir, libdir, dynlibdir, datadir, libexecdir, sysconfdir :: FilePath

bindir     = "/home/ale/.cabal/bin"
libdir     = "/home/ale/.cabal/lib/x86_64-linux-ghc-8.4.4/rattack-0.1.0.0-G479ttrdg0HD0SR1MniIGq"
dynlibdir  = "/home/ale/.cabal/lib/x86_64-linux-ghc-8.4.4"
datadir    = "/home/ale/.cabal/share/x86_64-linux-ghc-8.4.4/rattack-0.1.0.0"
libexecdir = "/home/ale/.cabal/libexec/x86_64-linux-ghc-8.4.4/rattack-0.1.0.0"
sysconfdir = "/home/ale/.cabal/etc"

getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir, getSysconfDir :: IO FilePath
getBinDir = catchIO (getEnv "rattack_bindir") (\_ -> return bindir)
getLibDir = catchIO (getEnv "rattack_libdir") (\_ -> return libdir)
getDynLibDir = catchIO (getEnv "rattack_dynlibdir") (\_ -> return dynlibdir)
getDataDir = catchIO (getEnv "rattack_datadir") (\_ -> return datadir)
getLibexecDir = catchIO (getEnv "rattack_libexecdir") (\_ -> return libexecdir)
getSysconfDir = catchIO (getEnv "rattack_sysconfdir") (\_ -> return sysconfdir)

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir ++ "/" ++ name)
