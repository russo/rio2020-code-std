{-# LANGUAGE Unsafe #-}
{-# LANGUAGE DataKinds #-}
module RunSum where

-- Definition of columns of the CSV file
import RowDef
-- Interface to run the query with a given epsilon
import CuratorLP (importCSV, dpEval)
 -- Analyses
import Sum

loadTraffic :: IO [Row]
loadTraffic = do
  ds <- importCSV "hotspot.csv"
  return $ toRows ds

execute1 = undefined

executeRouter1 = undefined
