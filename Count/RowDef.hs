{-
   Exercise: Dealing with sexual transmitted deseases (STD)

   We will use the file std.csv
-}
{-# LANGUAGE Trustworthy #-}
{-# LANGUAGE DataKinds #-}
module RowDef
   ( -- Types
     Row ()
   , Name
   , Lastname
   , STD
     -- Selectors
   , name
   , lastname
   , std
     -- Constructor
   , toRows
   )
where

type Name     = String
type Lastname = String
type STD      = String

data Row = Row {
                 name     :: Name
               , lastname :: Lastname
               , std      :: STD
               }
           deriving Show

toRow :: [String] -> Row
toRow [n, l, s] = Row {
                        name     = n
                      , lastname = l
                      , std      = s
                      }

toRow _ = error "Not correct format!"

toRows :: [[String]] -> [Row]
toRows csv = map toRow csv
